#include <stdio.h>
void first(int,int,int);
void second(int,int,int);
void third(int,int,int);
int main()
{
    int a,b,c;
    printf ("Enter 3 numbers\n");
    scanf ("%d%d%d",&a,&b,&c);
    first(a,b,c);
    second(a,b,c);
    third(a,b,c);
    return 0;
}
void first(int x,int y,int z)
{
    printf ("using simple if\n");
    if (x<y)
    {
        if (x<z)
        {
            printf ("%d is minimum\n",x);
        }
    }
    if (y<x)
    {
        if (y<z)
        {
            printf ("%d is minimum\n",y);
        }
    }
    if (z<x)
    {
        if (z<y)
        {
            printf ("%d is minimum\n",z);
        }
    }
}
void second(int x,int y,int z)
{
    printf ("using if else if\n");
    if ((x<y)&&(x<z))
    {
        printf ("%d is min\n",x);
    }
    else if ((y<x)&&(y<z))
    {
        printf ("%d is min\n",y);
    }
    else
    {
        printf ("%d is min\n",z);
    }

}
void third(int x,int y,int z)
{
    int d;
    d=(x<y)?((x<z)?x:z):((y<z)?y:z);
    printf ("%d is mimimum\n",d);
}