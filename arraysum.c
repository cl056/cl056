#include<stdio.h>
int input_n()
{
int a;
printf("Enter the number\n");
scanf("%d",&a);
return a;
}
void input_array(int n,int a[n])
{
int i;
for(i=0;i<n;i++)
{
printf("Enter the element no %d of the array\n",i);
scanf("%d",&a[i]);
}
}
int find_sum_array(int n,int a[n])
{
int i,sum=0;
for(i=0;i<n;i++)
{
sum=sum+a[i];
}
return sum;
}
void show_results(int n,int a[n],int sum)
{
int i;
printf("The sum of\n");
for(i=0;i<n-1;i++)
{
printf("%d+",a[i]);
}
printf("%d=%d",a[i],sum);
}
int main()
{
int n,sum;
n=input_n();
int a[n];
input_array(n,a);
sum=find_sum_array(n,a);
show_results(n,a,sum);
return 0;
}

